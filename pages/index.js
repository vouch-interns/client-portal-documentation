import Container from "../components/container"
import MoreStories from "../components/more-stories"
import HeroPost from "../components/hero-post"
import Guide from "../components/guide"
import Intro from "../components/intro"
import Layout from "../components/layout"
import { getAllPosts } from "../lib/api"
import Head from "next/head"
import { CMS_NAME } from "../lib/constants"

export default function Index({ allPosts }) {
  return (
    <>
      <Layout>
        <Head>
          <title>VCP Guide Documentation</title>
        </Head>
        <Container>
          <Intro />
          {allPosts &&
            allPosts.map(post => (
              <Guide
                key={post.slug}
                title={post.title}
                slug={post.slug}
                excerpt={post.excerpt}
              />
            ))}
        </Container>
      </Layout>
    </>
  )
}

export async function getStaticProps() {
  const allPosts = getAllPosts([
    "title",
    "date",
    "slug",
    "author",
    "coverImage",
    "excerpt",
  ])

  return {
    props: { allPosts },
  }
}
