import { CMS_NAME } from "../lib/constants"

export default function Intro() {
  return (
    <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
      <h3 className="text-2xl md:text-4xl font-bold tracking-tighter leading-tight md:pr-8">
        Vouch Documentation Portal
      </h3>
    </section>
  )
}
