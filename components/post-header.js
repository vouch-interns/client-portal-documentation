import PostTitle from "../components/post-title"

export default function PostHeader({ title, coverImage }) {
  return (
    <>
      <PostTitle>{title}</PostTitle>
    </>
  )
}
