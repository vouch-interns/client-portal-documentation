---
title: "VCP User Guide"
excerpt: "Guide for users of VCP"
coverImage: "/assets/blog/hello-world/cover.jpg"
author:
  name: Eden Ong
  picture: "/assets/blog/authors/tim.jpeg"
ogImage:
  url: "/assets/blog/hello-world/cover.jpg"
---

This is the user guide!

## Getting Started

### Logging In

To log in, head to [the Vouch login portal](https://portal-uat.vouch.sg/) and enter your login credentials. (Your username and password). You will be directed to different pages in the VCP, depending on whether your role is a [User](#user) or an [Admin](#admin). Click the links to view the user guides associated with each role.

### User

This will be the page that you will see once you log in as a User.
![User landing page](./images/user_landing.png)

### Admin

This will be the page that you will see once you log in as an Admin
![Admin landing page](./images/admin_landing.png)
